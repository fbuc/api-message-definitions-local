# Abbreviations and Definitions

Abbreviations are specified in upper case but also

| Definition | Abbreviation                |
|---         |---                          |
| BLFI       |  Bootloader & factory image |
| CE         |  Connected Equipment        |
| Config     |  Configuration              |
| DB         |  Daughterboard              |
| EP         |  Endpoint                   |
| FW         |  Firmware                   |
| HW         |  Hardware                   |
| MB         |  Motherboard                |
| MSG        |  Message                    |
| MCU        |  Microcontroller            |
| Ver        |  Version                    |
